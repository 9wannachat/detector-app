import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-time-main',
  templateUrl: './time-main.page.html',
  styleUrls: ['./time-main.page.scss'],
})
export class TimeMainPage implements OnInit {


  datetimeStart = null;
  datetimeEnd = null;
  public Id: any;
  public phase: any;
  public test: any;

  constructor(private router: Router, private datePicker: DatePicker, private route: ActivatedRoute,
              public alertController: AlertController) {
    this.Id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Please Select Time',
      buttons: ['OK']
    });

    await alert.present();
  }

  goChart() {

    if (this.datetimeStart === null || this.datetimeEnd === null) {
      this.presentAlert();
    } else {
      // this.presentAlert();
      // this.test = '/chart-main' + '/' + this.Id + '/' + this.datetimeStart + '/' + this.datetimeEnd;
      this.router.navigate(['/chart-main' + '/' + this.Id + '/' + this.datetimeStart + '/' + this.datetimeEnd]);
    }
  }

  startTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        let m;
        if (date.getMonth() === 12) {
          m = 1;
        } else {
          m = date.getMonth() + 1;
        }

        this.datetimeStart = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        console.log('Got date: ', this.datetimeStart);

      },
      err => {
        console.log('Error occurred while getting date: ', err);
      }
    );
  }

  endTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        let m;
        if (date.getMonth() === 12) {
          m = 1;
        } else {
          m = date.getMonth() + 1;
        }

        this.datetimeEnd = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        console.log('Got date: ', this.datetimeEnd);

      },
      err => {
        console.log('Error occurred while getting date: ', err);
      }
    );
  }


}
