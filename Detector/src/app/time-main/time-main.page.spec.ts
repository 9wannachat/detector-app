import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeMainPage } from './time-main.page';

describe('TimeMainPage', () => {
  let component: TimeMainPage;
  let fixture: ComponentFixture<TimeMainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeMainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
