import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
declare var google;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  map: any;
  @ViewChild('mapElement', { static: true }) mapElement: ElementRef;
  stateMark = false;
  marker: any;
  mapState = false;

  markerArray = [];

  public usrId: any;

  constructor(public alertController: AlertController, private router: Router, private http: HttpClient, private storage: Storage) {

  }



  ngOnInit(): void {

    this.storage.get('idUser').then((val) => {
      this.http.get('http://167.71.222.155:8080/api/device/' + val).subscribe(res => {
        const data = JSON.parse(JSON.stringify(res));
        this.loadMap(data.data);

      });
    });
  }

  loadMap(data) {
    this.map = new google.maps.Map(
      this.mapElement.nativeElement, {
      center: { lat: 7.1545593, lng: 100.6223298 },
      zoom: 6
    }
    );
    console.log(data);

    const objKey = data.length;
    // console.log(objKey);
    for (let i = 0; i < objKey; i++) {
      // console.log(data[i].lat + '' + data[i].lng);
      const marker = new google.maps.Marker({
        position: { lat: data[i].lat, lng: data[i].lng },
        title: 'Hello World!'
      });
      let self = this;
      google.maps.event.addListener(marker, 'click', function (event, objKey) {
        console.log(objKey);
        self.router.navigate(['/subdevices/' + data[i].idDevice]);
      });

      marker.setMap(this.map);
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  goDevices() {
    this.router.navigate(['/devices']);
  }

  goControl() {
    this.router.navigate(['/controll']);
  }

}
