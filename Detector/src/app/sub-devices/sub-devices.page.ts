import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-sub-devices',
  templateUrl: './sub-devices.page.html',
  styleUrls: ['./sub-devices.page.scss'],
})
export class SubDevicesPage implements OnInit {

  public Id: any;
  public item: any;

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient, public alertController: AlertController,
    private storage: Storage) {
    this.Id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getData();
  }

  getData() {

    this.storage.get('idUser').then((val) => {
      this.http.get('http://167.71.222.155:8080/api/sub-device/' +  val + '/' + this.Id).subscribe(res => {
        const data = JSON.parse(JSON.stringify(res));
        this.item = data.data;
      });
    });
  }

  async presentAlert(id, p) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: id + '' + p,
      buttons: ['OK']
    });

    await alert.present();
  }

  goChartView(devices) {
    //  this.presentAlert( devices.id, devices.phase);
    this.router.navigate(['/setting-time/' + devices.id + '/' + devices.phase]);
  }

}
