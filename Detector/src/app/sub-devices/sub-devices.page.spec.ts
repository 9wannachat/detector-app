import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDevicesPage } from './sub-devices.page';

describe('SubDevicesPage', () => {
  let component: SubDevicesPage;
  let fixture: ComponentFixture<SubDevicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubDevicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDevicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
