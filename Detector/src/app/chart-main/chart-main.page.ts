import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-chart-main',
  templateUrl: './chart-main.page.html',
  styleUrls: ['./chart-main.page.scss'],
})
export class ChartMainPage implements OnInit {

  @ViewChild('lineCanvas', { static: true }) lineCanvas: ElementRef;
  private lineChart: Chart;

  public Id: any;
  public Start: any;
  public End: any;

  public powerX = [];
  public phase1 = [];
  public phase2 = [];
  public phase3 = [];

  public colorLine = '#706f6d';
  public colorPhase1 = '#FE7A15';
  public colorPhase2 = '#94B447';
  public colorPhase3 = '#0191B4';

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.Id = this.route.snapshot.paramMap.get('id');
    this.Start = this.route.snapshot.paramMap.get('start');
    this.End = this.route.snapshot.paramMap.get('end');
  }

  ngOnInit() {
    this.getData();
  }

  ngAfterContentInit(): void {
    this.power();
  }

  getData() {
    this.http.get('http://167.71.222.155:8080/api/log/ac/' + this.Id + '/' + this.Start + '/' + this.End).subscribe(response => {

      const data = JSON.parse(JSON.stringify(response));

      // console.log(data);

      if (data.idStatus === 200) {
        const key = Object.keys(data.phase1);
        for (let x = 0; x < key.length; x++) {
          this.powerX.push(data.dt[x]);
        }

        for (let x = 0; x < key.length; x++) {
          this.phase1.push(data.phase1[x]);
        }

        for (let x = 0; x < key.length; x++) {
          this.phase2.push(data.phase2[x]);
        }

        for (let x = 0; x < key.length; x++) {
          this.phase3.push(data.phase3[x]);
        }
      }
    });
  }

  power() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.powerX,
        datasets: [
          {
            label: 'Phase 1',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorPhase1,
            borderColor: this.colorPhase1,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: this.colorPhase1,
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 1,
            pointHoverRadius: 1,
            pointHoverBackgroundColor: this.colorPhase1,
            pointHoverBorderColor: this.colorPhase1,
            pointHoverBorderWidth: 1,
            pointRadius: 1,
            pointHitRadius: 5,
            data: this.phase1,
            spanGaps: false
          },
          {
            label: 'Phase 2',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorPhase2,
            borderColor: this.colorPhase2,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: this.colorPhase2,
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 1,
            pointHoverRadius: 1,
            pointHoverBackgroundColor: this.colorPhase2,
            pointHoverBorderColor: this.colorPhase2,
            pointHoverBorderWidth: 1,
            pointRadius: 1,
            pointHitRadius: 5,
            data: this.phase2,
            spanGaps: false
          },
          {
            label: 'Phase 3',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorPhase3,
            borderColor: this.colorPhase3,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: this.colorPhase3,
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 1,
            pointHoverRadius: 1,
            pointHoverBackgroundColor: this.colorPhase3,
            pointHoverBorderColor: this.colorPhase3,
            pointHoverBorderWidth: 1,
            pointRadius: 1,
            pointHitRadius: 5,
            data: this.phase3,
            spanGaps: false
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            gridLines: {
              color: 'rgba(171,171,171,0.3)',
              lineWidth: 1
            },
            ticks: {
              maxTicksLimit: 5,
              autoSkip: true
            },
            display: true
          }],
          yAxes: [{
            gridLines: {
              color: 'rgba(171,171,171,0.3)',
              lineWidth: 0.5
            },
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

}
