import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartMainPage } from './chart-main.page';

describe('ChartMainPage', () => {
  let component: ChartMainPage;
  let fixture: ComponentFixture<ChartMainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartMainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
