import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.page.html',
  styleUrls: ['./chart-view.page.scss'],
})
export class ChartViewPage implements OnInit {

  @ViewChild('lineCanvas', { static: true }) lineCanvas: ElementRef;
  private lineChart: Chart;
  public Id: any;
  public phase: any;
  public start: any;
  public end: any;

  colorBrown = '#a37207';
  colorBlack = '#030200';
  colorGray = '#706f6d';
  colorDC = '#e8651a';
  public colorLine = '#706f6d';

  public powerX = [];
  public powerY = [];

  public typeChart: any;
  public dataChart: any;
  public optionsChart: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.Id = this.route.snapshot.paramMap.get('id');
    this.phase = this.route.snapshot.paramMap.get('phase');
    this.start = this.route.snapshot.paramMap.get('start');
    this.end = this.route.snapshot.paramMap.get('end');
  }

  getData() {
    this.http.get('http://167.71.222.155:8080/api/log/' + this.Id + '/' + this.start + '/' + this.end).subscribe(response => {

      const data = JSON.parse(JSON.stringify(response));

      if (data.idStatus === 200) {
        const key = Object.keys(data.data);

        const powerStr = JSON.stringify(data.data);
        const power = JSON.parse(powerStr);

        for (let x = 0; x < key.length; x++) {
          this.powerX.push(power[x].dt);
        }

        for (let y = 0; y < key.length; y++) {
          this.powerY.push(power[y].power);
        }
      }
    });

  }

  power() {

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.powerX,
        datasets: [
          {
            label: 'Power',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorLine,
            borderColor: this.colorLine,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: this.colorLine,
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 1,
            pointHoverRadius: 1,
            pointHoverBackgroundColor: this.colorLine,
            pointHoverBorderColor: this.colorLine,
            pointHoverBorderWidth: 1,
            pointRadius: 1,
            pointHitRadius: 5,
            data: this.powerY,
            spanGaps: false
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            gridLines: {
              color: 'rgba(171,171,171,0.3)',
              lineWidth: 1
            },
            ticks: {
              maxTicksLimit: 5,
              autoSkip: true
            },
            display: true
          }],
          yAxes: [{
            gridLines: {
              color: 'rgba(171,171,171,0.3)',
              lineWidth: 0.5
            },
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

  }

  ngAfterViewInit(): void {
    this.power();

  }

  ngOnInit() {

    this.getData();
    this.power();
    if (this.phase === 1) {
      this.colorLine = this.colorBrown;
    } else if (this.phase === 2) {
      this.colorLine = this.colorBlack;
    } else if (this.phase === 3) {
      this.colorLine = this.colorGray;
    } else {
      this.colorLine = this.colorDC;
    }
  }

}
