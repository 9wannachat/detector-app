import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartViewPage } from './chart-view.page';

describe('ChartViewPage', () => {
  let component: ChartViewPage;
  let fixture: ComponentFixture<ChartViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
