import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingTimePage } from './setting-time.page';

describe('SettingTimePage', () => {
  let component: SettingTimePage;
  let fixture: ComponentFixture<SettingTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingTimePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
