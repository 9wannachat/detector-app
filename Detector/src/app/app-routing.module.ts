import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'devices', loadChildren: './devices/devices.module#DevicesPageModule' },
  { path: 'subdevices/:id', loadChildren: './sub-devices/sub-devices.module#SubDevicesPageModule' },
  { path: 'chart-view/:id/:phase/:start/:end', loadChildren: './chart-view/chart-view.module#ChartViewPageModule' },
  { path: 'setting-time/:id/:phase', loadChildren: './setting-time/setting-time.module#SettingTimePageModule' },
  { path: 'time-main/:id', loadChildren: './time-main/time-main.module#TimeMainPageModule' },
  { path: 'chart-main/:id/:start/:end', loadChildren: './chart-main/chart-main.module#ChartMainPageModule' },
  { path: 'controll', loadChildren: './controll/controll.module#ControllPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
