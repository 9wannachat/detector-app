import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public usr = null;
  public pass = null;

  constructor(private router: Router, public toastController: ToastController, private http: HttpClient, private storage: Storage) { }

  ngOnInit() {
  }

  signUp() {
    this.router.navigate(['/signup']);
  }

  logIn() {

    if (this.usr != null && this.pass != null) {

      const body = {
        username: this.usr,
        password: this.pass
      };

      this.http.post('http://167.71.222.155:8080/api/user/login', body).subscribe( res => {
          const raw = JSON.parse(JSON.stringify(res));
          if (raw.idStatus === 200) {
            this.storage.set('idUser', raw.id);
            this.router.navigate(['/home']);
          } else {
            const data = 'Username or Password Invalid';
            this.presentToast(data);
          }
      });

    } else {
      const data = 'Enter Username or Password';
      this.presentToast(data);
    }

   
  }

  async presentToast(data) {
    const toast = await this.toastController.create({
      message: data,
      duration: 2000
    });
    toast.present();
  }

}
