import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-devices',
  templateUrl: './devices.page.html',
  styleUrls: ['./devices.page.scss'],
})
export class DevicesPage implements OnInit {

  public item: any;
  public timeInterval: any

  constructor(private router: Router, private http: HttpClient, private storage: Storage) { }

  ngOnInit() {
    this.getData();
  }

  ngAfterContentInit(): void {
    setInterval(() => {
      this.getData();
    }, 3000);
  }

  getData() {
    this.storage.get('idUser').then((val) => {
      this.http.get('http://167.71.222.155:8080/api/device/' + val).subscribe(res => {
        const raw = JSON.parse(JSON.stringify(res));
        this.item = raw.data;
      });
    });
   
  }

  goHome() {
    this.router.navigate(['/home']);
  }

  goSubDevices(id) {
    this.router.navigate(['/subdevices/' + id]);
  }

  goSetting(id) {
    this.router.navigate(['/time-main/' + id]);
  }
}
