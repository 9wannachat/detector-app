import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-controll',
  templateUrl: './controll.page.html',
  styleUrls: ['./controll.page.scss'],
})
export class ControllPage implements OnInit {

  constructor(private http: HttpClient, public alertController: AlertController) { }

  ngOnInit() {
  }

  async presentAlert(data) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }

  controllOn(id, status) {
    // this.presentAlert('id'+ id + 'status' + status);
    this.http.get('http://167.71.222.155:3333?status=' + status + '&gpio=' + id).subscribe(res => {
      const data = JSON.parse(JSON.stringify(res));
      this.presentAlert(data);
    });
  }



}
