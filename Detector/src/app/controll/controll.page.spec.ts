import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllPage } from './controll.page';

describe('ControllPage', () => {
  let component: ControllPage;
  let fixture: ComponentFixture<ControllPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
